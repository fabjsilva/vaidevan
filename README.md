# PROJETO INTEGRADOR - FATEC -São Jose do Campos
## Grupo Mobteam
* Anderson Oliveira da Silva Junior 
* Carlos Fernando de Souza
* Danilo Silva Lima
* Douglas Hiromi Nishiama (Master 6º Semestre)
* Fábio José da Silva 
* Felipe Gustavo Pais Pereira 
* Jéssica Helen Helbusto Rosado (Master 6º Semestre)
* Leonardo Souza Ferrianci 
* Michelle Cristine dos Santos  
# Aplicativo Vaidevan - Necessidade
O cliente é um motorista autonomo de Van, que necessita de um aplicativo que controle o  serviço de transporte de seus passageiros(clientes fixos e pré-cadastrados)em seu trajeto  pré-determinado, tanto na ida quanto na volta, durante a semana(segunda a sexta feira)
## Requisitos
 **São requisitos obrigatório exigidos pelo cliente:**
1. Gerenciamento de destino. Cada destino possui descrição, local, horário de chegada e horário de volta.Disponível apenas para o motorista.
2. Gerenciamento de passageiros. Cada passageiro tem nome, telefone de contato, endereço e um ou mais destinos. Disponível apenas para o motorista. 
3. Definição de rota. A partir de um ponto de saída e um destino, o app sugere a ordem em que os passageiros devem ser apanhados. O motorista pode definir, de acordo com sua experiência e condições de trânsito o horário estimado em que chegará na localidade de cada passageiro.
4. Função "hora prevista". O app deve informar ao passageiro a hora prevista de chegada da van (na ida) e qual o horário previsto para entrega (na volta).
5. Utilizar o APP INVENTOR
# Datas dos Sprints:
1. -****Sprint 0 - Início: 07/09/2020 Entrega: 27/09/2020 (Executado)****
2. -****Sprint 1 - Início: 28/09/2020 Entrega: 18/10/2020****
3. -****Sprint 2 - Início: 19/10/2020 Entrega: 08/11/2020****
4. -****Sprint 3 - Início: 09/11/2020 Entrega: 29/11/2020****
5. -****Apresentação final: 30/11/2020 a 06/12/2020****

# Tecnologia

* App Inventor - Aplicativo de código aberto, originalmente criado pala Google,hoje é mantido pelo Massachusetts Institute of Technology(MIT). É um ambiente de programação visual baseada em blocos que facilita a criação de aplicativos complexos até para uma pessoa leiga.
* MIT AI2 Companion - Aplicativo emulador para Android.
* aiStater - Aplicativo aiStarter (emulador de Sistema Android para PC).

# Sprint 0 -- Tela inicial de cadastro - 
* **Demonstração da tela inicial com login no aplicativo**
* **Demonstração do cadastro inicial do usuarios (Motorista e Passageiros)**

# Sprint 1 -
1. **Entrega da tela de gerenciar destinos**
2. **Criada tela de cadastro de destino**
3. **Motorista - É possível fazer o cadastro, alterar e excluir o destino de um passageiro.**
4. **São listados os destinos cadastrado através de consulta no banco Firebase.**
5. **Passageiro - É possível fazer apenas o cadastro do destino.**
# Sprint 2 -

# Sprint 3 -
